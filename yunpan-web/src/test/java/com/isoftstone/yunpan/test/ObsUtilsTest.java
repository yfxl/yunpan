package com.isoftstone.yunpan.test;

import com.isoftstone.YunpanApplication;
import com.isoftstone.yunpan.utils.ObsUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/12 11:14
 * @Version: 1.0
 * @Description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes= YunpanApplication.class)//参数配置启动类，不是当前测试类对象
public class ObsUtilsTest {
    @Test
    public void test_createBucket(){
        ObsUtils obsUtils=new ObsUtils();
        String bucketName=obsUtils.createBucket("base-bucket-01");
       System.out.println(bucketName);
    }
    @Test
    public void test_showBuckets(){
        ObsUtils obsUtils=new ObsUtils();
        List<String> list=obsUtils.showBuckets();
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }

    @Test
    public void test_createDirs(){
        ObsUtils obsUtils=new ObsUtils();
        String str=obsUtils.createDirs("base-bucket-01","emp-dirs/imags");
        System.out.println(str);
    }
    @Test
    public void test_upload(){
        ObsUtils obsUtils=new ObsUtils();
        File file=new File("C:/Users/Administrator/Downloads/credentials (5).csv");
        FileInputStream inputStream= null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        obsUtils.uploadFile("base-bucket-01","emp-dirs","credentials (5).csv",inputStream,file.length());

    }
}
