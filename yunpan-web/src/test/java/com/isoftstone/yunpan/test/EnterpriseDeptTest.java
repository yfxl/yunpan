package com.isoftstone.yunpan.test;

import com.isoftstone.YunpanApplication;
import com.isoftstone.yunpan.framework.entity.SqlWhere;
import com.isoftstone.yunpan.system.dao.EnterpriseDeptDao;
import com.isoftstone.yunpan.system.entity.EnterpriseDeptEntity;
import com.isoftstone.yunpan.system.service.EnterpriseDeptService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/12 9:59
 * @Version: 1.0
 * @Description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes= YunpanApplication.class)//参数配置启动类，不是当前测试类对象
public class EnterpriseDeptTest {
    @Autowired
    private EnterpriseDeptService enterpriseDeptService;
    @Test
    public void test_insert(){
        EnterpriseDeptEntity entity=new EnterpriseDeptEntity();
        entity.setDeptNo("1001");
        entity.setDeptName("教学教研");
        entity.setCreateDate("20210707");
        entity.setCreateTime("112340");
        entity.setCreateUser("100001");
        entity.setObsDir("/sdf");
        entity.setCompanyName("北京软通动力教育科技有限公司");
        int i= 0;
        try {
            i = enterpriseDeptService.insertEntity(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(i);
    }
    @Test
    public void test_selectList(){
        try {
           SqlWhere sqlWhere= new SqlWhere();
           sqlWhere.setSqlWhere(new StringBuffer(" and DEPTNAME like '%智慧%'"));
            List<EnterpriseDeptEntity> list= enterpriseDeptService.selectList(sqlWhere);
            for(int i=0;i<list.size();i++){
                System.out.println(list.get(i).getDeptId()+":"+list.get(i).getDeptName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void test_selectById(){
        try {
             EnterpriseDeptEntity entity= enterpriseDeptService.selectById("1");
             System.out.println(entity.getDeptId()+":"+entity.getDeptName());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Test
    public void test_updateById(){
        try {
        EnterpriseDeptEntity entity= enterpriseDeptService.selectById("1");
        entity.setCompanyName("北京软通智慧科技有限公司");
        entity.setDeptNo("20000");
        int i=enterpriseDeptService.updateEntity(entity);
            System.out.println(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_deleteById(){
        try{
            int i=enterpriseDeptService.deleteById("1");
            System.out.println(i);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
