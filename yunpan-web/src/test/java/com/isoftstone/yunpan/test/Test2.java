package com.isoftstone.yunpan.test;

import com.isoftstone.YunpanApplication;
import com.isoftstone.yunpan.demo.service.DemoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Timed;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/9 16:03
 * @Version: 1.0
 * @Description:
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes= YunpanApplication.class)//参数配置启动类，不是当前测试类对象
public class Test2 {
    @Autowired
    private DemoService demoService;
    @Test
    public void test_insert(){
        System.out.println("service中方法被执行：");
        demoService.uploadDemo();
    }
}
