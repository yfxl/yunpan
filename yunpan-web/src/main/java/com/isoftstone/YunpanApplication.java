package com.isoftstone;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
//用于启动springboot
@SpringBootApplication
@MapperScan(/*{"com.isoftstone.yunpan.demo.dao","com.isoftstone.yunpan.system.dao"}*/
        markerInterface =com.isoftstone.yunpan.framework.dao.BaseDao.class,basePackages = {"com.isoftstone.yunpan"})//扫描该目录下所有接口，映射Mybatis 的mapper xml文件
public class  YunpanApplication {

    public static void main(String[] args) {
        SpringApplication.run( YunpanApplication.class, args);
    }

}
