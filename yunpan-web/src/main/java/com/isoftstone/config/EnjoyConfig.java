package com.isoftstone.config;

import com.jfinal.template.source.ClassPathSourceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/***
 *
 * 使用自定义的视图解析器
 */
@Configuration
public class EnjoyConfig {
    @Bean(name = "myViewResolver")
    public MyViewResolver getJFinalViewResolver() {
        MyViewResolver jf = new MyViewResolver();//自定义的视图解析器
        jf.setDevMode(true);
        jf.setSourceFactory(new ClassPathSourceFactory());
        //这里根据自己的目录修改，一般页面放到/templates下面
        jf.setBaseTemplatePath("/templates/");
        jf.setSuffix(".html");
        jf.setContentType("text/html;charset=UTF-8");
        jf.setOrder(0);
        return jf;
    }
}
