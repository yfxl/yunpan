package com.isoftstone.config;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

@Configuration
public class MybatisDbConfigure {

	protected static Log log = LogFactory.getLog(MybatisDbConfigure.class);

	@Bean
	@ConfigurationProperties(prefix = "mybatis.configuration")
	public org.apache.ibatis.session.Configuration globalConfig() {
		return new org.apache.ibatis.session.Configuration();
	}

	@Bean(name = "sqlSessionFactoryBean")
	public SqlSessionFactoryBean myGetSqlSessionFactory(DataSource dataSource,
			org.apache.ibatis.session.Configuration config) throws Exception {

		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();

		// mapperLocations

		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

		// dataSource

		sqlSessionFactoryBean.setDataSource(dataSource);
		sqlSessionFactoryBean.setConfiguration(config);
		sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:mapper/*.xml"));

		//SqlSessionFactory sessionFactory = sqlSessionFactoryBean.getObject();

		return sqlSessionFactoryBean;

	}

}
