package com.isoftstone.config;

import com.jfinal.template.ext.spring.JFinalViewResolver;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/***
 * 视图解析器
 */
public class MyViewResolver extends JFinalViewResolver implements
        ApplicationListener<ContextRefreshedEvent> {

    /**
     * 添加默认路径
     *
     * @param contextRefreshedEvent
     */
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        getEngine().addSharedObject("cxp", getServletContext().getContextPath());
    }
}
