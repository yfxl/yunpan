package com.isoftstone.config;

import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

import javax.servlet.MultipartConfigElement;

@Configuration
public class InitConfig{
@Bean
public MultipartConfigElement multipartConfigElement() {
    MultipartConfigFactory factory = new MultipartConfigFactory();
    //文件最大10M,DataUnit提供5中类型B,KB,MB,GB,TB
    factory.setMaxFileSize(DataSize.of(5, DataUnit.GIGABYTES));
    factory.setMaxRequestSize(DataSize.of(20, DataUnit.GIGABYTES));
    return factory.createMultipartConfig();
}

}
