package com.isoftstone.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/11 22:24
 * @Version: 1.0
 * @Description:
 */
@Controller
public class DemoControl {

    @RequestMapping("/test_1")//用于定义请求URL
    public String test_1(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {

        return "/test_1";
    }

    @RequestMapping("/")
    public String test_2(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {

        return "/login";
    }

    @RequestMapping("/index")
    public String test_3(Model model, String username, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        model.addAttribute("username", "smith");
        return "/index";
    }

    @RequestMapping("/admin")
    public String test_4(Model model, String username, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        model.addAttribute("username", "admin");
        return "/admin";
    }



    @RequestMapping("/myshare")
    public String myshare(Model model, String username, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        model.addAttribute("username", "smith");
        return "/myshare";
    }

    @RequestMapping("/dcofile")
    public String dcofile(Model model, String username, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        model.addAttribute("username", "smith");
        return "/dcofile";
    }

    //用户容量页
    @RequestMapping("/distribution")
    public String distribution(Model model, String username, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        model.addAttribute("username", "smith");
        return "/distribution";
    }

    //图片文件列表页
    @RequestMapping("/gallery")
    public String gallery(Model model, String username, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        model.addAttribute("username", "smith");
        return "/gallery";
    }

    //重置密码页
    @RequestMapping("/recoverpw")
    public String recoverpw(Model model, String username, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        model.addAttribute("username", "smith");
        return "/recoverpw";
    }

    //注册页
    @RequestMapping("/register")
    public String register(Model model, String username, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        model.addAttribute("username", "smith");
        return "/register";
    }

    //桶列表页
    @RequestMapping("/serach")
    public String serach(Model model, String username, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) {
        model.addAttribute("username", "smith");
        return "/serach";
    }

}