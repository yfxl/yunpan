package com.isoftstone.yunpan.system.service;

import com.isoftstone.yunpan.framework.entity.SqlWhere;
import com.isoftstone.yunpan.system.entity.EnterpriseDeptEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/12 10:11
 * @Version: 1.0
 * @Description:
 */

public interface EnterpriseDeptService {
    public int insertEntity(EnterpriseDeptEntity entity) throws Exception;
    public int updateEntity(EnterpriseDeptEntity entity) throws Exception ;
    public int deleteById(String id) throws Exception;
    public EnterpriseDeptEntity selectById(String id) throws Exception;
    public List<EnterpriseDeptEntity> selectList(SqlWhere sqlParams) throws Exception;
}
