package com.isoftstone.yunpan.system.service;

import com.isoftstone.yunpan.framework.entity.SqlWhere;
import com.isoftstone.yunpan.system.dao.EnterpriseDeptDao;
import com.isoftstone.yunpan.system.entity.EnterpriseDeptEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/12 10:12
 * @Version: 1.0
 * @Description:
 */
@Service
public class EnterpriseDeptServiceImpl implements EnterpriseDeptService {
    @Autowired
    private EnterpriseDeptDao enterpriseDeptDao;
    public int insertEntity(EnterpriseDeptEntity entity) throws Exception {
        return enterpriseDeptDao.insertEntity(entity);
    }

    public int updateEntity(EnterpriseDeptEntity entity) throws Exception {
        return enterpriseDeptDao.updateEntity(entity);
    }

    public int deleteById(String id) throws Exception {
        return enterpriseDeptDao.deleteById(id);
    }

    public EnterpriseDeptEntity selectById(String id) throws Exception {
        return enterpriseDeptDao.selectById(id);
    }

    public List<EnterpriseDeptEntity> selectList(SqlWhere sqlParams) throws Exception {
        return enterpriseDeptDao.selectList(sqlParams);
    }
}
