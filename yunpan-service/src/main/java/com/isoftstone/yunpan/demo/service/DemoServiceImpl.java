package com.isoftstone.yunpan.demo.service;

import com.isoftstone.yunpan.demo.dao.DemoDao;
import com.isoftstone.yunpan.demo.entity.DemoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/9 14:39
 * @Version: 1.0
 * @Description:
 */

@Service//该类需要通过spring实例化@Repository 用于dao层实例化对象，@Controller用于控制层
public class DemoServiceImpl implements DemoService {
    @Autowired//将实例化好的对象注入给demoDao属性
    private DemoDao demoDao;
    //上传文件操作（1）文件格式校验（2）上传（3）部分数据库持久化到数据库
    public void uploadDemo(){
        System.out.println("文件检查");
        System.out.println("文件上传");
        DemoEntity entity=new DemoEntity();
        entity.setDemoName("JONES");
        entity.setId("1006");

        try {
            demoDao.insertEntity(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
