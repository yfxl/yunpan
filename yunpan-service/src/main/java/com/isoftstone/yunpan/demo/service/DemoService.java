package com.isoftstone.yunpan.demo.service;

import com.isoftstone.yunpan.demo.dao.DemoDao;
import com.isoftstone.yunpan.demo.entity.DemoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/9 14:39
 * @Version: 1.0
 * @Description:
 */

public interface DemoService   {

    public void uploadDemo();


}
