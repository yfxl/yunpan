package com.isoftstone.yunpan.utils;

import com.obs.services.ObsClient;
import com.obs.services.exception.ObsException;
import com.obs.services.model.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/12 10:59
 * @Version: 1.0
 * @Description:
 * 操作OBS桶以及资源工具类
 */
public class ObsUtils {
    LogManager manager=LogManager.getLogManager();
    private static final String endPoint = "https://obs.cn-north-4.myhuaweicloud.com";
    private static final String ak = "NMERCCCMVUZJSXRXGEZ4";
    private static final String sk = "Jmh4LLO4QiQFZw5DWpzuQnEzdmudsKtTuK4yPsIq";
    //创建桶;返回桶信息
    public String createBucket(String bucketName) {
        // 创建ObsClient实例
        ObsClient obsClient = new ObsClient(ak, sk, endPoint);
        ObsBucket obsBucket = new ObsBucket();
        // 设置桶区域位置
        obsBucket.setLocation("cn-north-4");//北京一不需要设置，其他必须设置，此处obs应用的是北京四
        // 设置桶访问权限为公共读，默认是私有读写//AccessControlList.REST_CANNED_PUBLIC_READ
        obsBucket.setAcl(AccessControlList.REST_CANNED_PRIVATE);//私有
        // 设置桶的存储类型为标准存储的；COLD则为归档存储
        obsBucket.setBucketStorageClass(StorageClassEnum.STANDARD);
        boolean exists = obsClient.headBucket(bucketName);

        // 创建桶
        try {
            if (!exists) {
                obsBucket.setBucketName(bucketName);
                // 创建桶成功
                HeaderResponse response = obsClient.createBucket(obsBucket);
                System.out.println(response.getRequestId());
            } else {
                // System.out.println("桶已经存在");
                manager.getLogger("桶已存在！");
                return null;
            }

        } catch (ObsException e) {
            e.printStackTrace();
            // 创建桶失败
            System.out.println("HTTP Code: " + e.getResponseCode());
            System.out.println("Error Code:" + e.getErrorCode());
            System.out.println("Error Message: " + e.getErrorMessage());
            System.out.println("Request ID:" + e.getErrorRequestId());
            System.out.println("Host ID:" + e.getErrorHostId());
            return null;
        } finally {

            if (obsClient != null) {
                try {
                    obsClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bucketName;
    }


    //查看桶列表;返回桶信息
    public List<String> showBuckets() {
        // 创建ObsClient实例
        ObsClient obsClient = new ObsClient(ak, sk, endPoint);
        ListBucketsRequest request = new ListBucketsRequest();
        request.setQueryLocation(true);
        List<ObsBucket> buckets = obsClient.listBuckets(request);
        List<String>list=null;
        if(buckets!=null&&buckets.size()>0){
            list=new ArrayList<String>();
            for(ObsBucket bucket : buckets){
                list.add(bucket.getBucketName());
                /*System.out.println("BucketName:" + bucket.getBucketName());
                System.out.println("CreationDate:" + bucket.getCreationDate());
                System.out.println("Location:" + bucket.getLocation());*/
            }
        }
        if (obsClient != null) {
            try {
                obsClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return list;
    }
    //在某个桶中创建文件夹；返回文件夹信息
    public String createDirs(String bucketName,String dirs){
        // 创建ObsClient实例
        ObsClient obsClient = new ObsClient(ak, sk, endPoint);
        try {
            obsClient.putObject(bucketName, dirs, new ByteArrayInputStream(new byte[0]));
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }finally {
            if (obsClient != null) {
                try {
                    obsClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return dirs;
    }

    //在某一个桶的某个目录下，创建文件夹，返回文件夹信息(同上，暂时无需实现)
    //上传文件返回上传文件存储地址
    public Map<String,Object> uploadFile(final String bucketName,  final String dir, final String fileName, final InputStream fileInputStream, final long fileSize){
        // 创建ObsClient实例
        final ObsClient obsClient = new ObsClient(ak, sk, endPoint);


// 初始化线程池
        ExecutorService executorService = Executors.newFixedThreadPool(5);


// 初始化分段上传任务
        InitiateMultipartUploadRequest request = new InitiateMultipartUploadRequest(bucketName, dir+"/"+fileName);

        InitiateMultipartUploadResult result = obsClient.initiateMultipartUpload(request);

        final String uploadId = result.getUploadId();
        System.out.println("\t"+ uploadId + "\n");

// 每段上传100MB
        long partSize = 100 * 1024 * 1024L;


// 计算需要上传的段数
        long partCount = fileSize % partSize == 0 ? fileSize / partSize : fileSize / partSize + 1;

        final List<PartEtag> partEtags = Collections.synchronizedList(new ArrayList<PartEtag>());

// 执行并发上传段
        for (int i = 0; i < partCount; i++)
        {
            // 分段在文件中的起始位置
            final long offset = i * partSize;
            // 分段大小
            final long currPartSize = (i + 1 == partCount) ? fileSize - offset : partSize;
            // 分段号
            final int partNumber = i + 1;
            executorService.execute(
            new Runnable()
            {
                public void run()
                {
                    UploadPartRequest uploadPartRequest = new UploadPartRequest();
                    uploadPartRequest.setBucketName(bucketName);
                    uploadPartRequest.setObjectKey(fileName);
                    uploadPartRequest.setUploadId(uploadId);
                    uploadPartRequest.setInput(fileInputStream);
                    uploadPartRequest.setPartSize(currPartSize);
                    uploadPartRequest.setOffset(offset);
                    uploadPartRequest.setPartNumber(partNumber);

                    UploadPartResult uploadPartResult;
                    try
                    {
                        uploadPartResult = obsClient.uploadPart(uploadPartRequest);
                        System.out.println("Part#" + partNumber + " done\n");
                        partEtags.add(new PartEtag(uploadPartResult.getEtag(), uploadPartResult.getPartNumber()));
                    }
                    catch (ObsException e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }

// 等待上传完成
        executorService.shutdown();
        while (!executorService.isTerminated())
        {
            try
            {
                executorService.awaitTermination(5, TimeUnit.SECONDS);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
// 合并段
        CompleteMultipartUploadRequest completeMultipartUploadRequest = new CompleteMultipartUploadRequest(bucketName, fileName, uploadId, partEtags);
        obsClient.completeMultipartUpload(completeMultipartUploadRequest);
      return null;
    }
    //下载返回文件地址
}
