package com.isoftstone.yunpan.framework.entity;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/13 8:13
 * @Version: 1.0
 * @Description:
 */
public class SqlWhere {
    private StringBuffer sqlWhere;

    public StringBuffer getSqlWhere() {
        return sqlWhere;
    }

    public void setSqlWhere(StringBuffer sqlWhere) {
        this.sqlWhere = sqlWhere;
    }
}
