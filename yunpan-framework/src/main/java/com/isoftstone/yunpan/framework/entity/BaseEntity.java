package com.isoftstone.yunpan.framework.entity;

import java.io.Serializable;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/9 14:00
 * @Version: 1.0
 * @Description:所有试题类必须继承BaseEntity
 */
public class BaseEntity implements Serializable {

}
