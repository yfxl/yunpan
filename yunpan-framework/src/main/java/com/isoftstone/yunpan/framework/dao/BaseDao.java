package com.isoftstone.yunpan.framework.dao;

import com.isoftstone.yunpan.framework.entity.BaseEntity;
import com.isoftstone.yunpan.framework.entity.SqlWhere;

import java.util.List;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/9 13:59
 * @Version: 1.0
 * @Description:所有Dao 接口必须继承BaseDao
 */
public interface BaseDao<T extends BaseEntity> {
   public int insertEntity(T entity) throws Exception;
   public int updateEntity(T entity) throws Exception ;
   public int deleteById(String id) throws Exception;
   public T selectById(String id) throws Exception;
   public List<T> selectList(SqlWhere sqlParams) throws Exception;
}
