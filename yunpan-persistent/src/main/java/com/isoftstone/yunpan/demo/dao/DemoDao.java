package com.isoftstone.yunpan.demo.dao;

import com.isoftstone.yunpan.demo.entity.DemoEntity;
import com.isoftstone.yunpan.framework.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/9 14:19
 * @Version: 1.0
 * @Description:
 */
@Mapper
public interface DemoDao extends BaseDao<DemoEntity> {

}
