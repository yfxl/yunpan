package com.isoftstone.yunpan.demo.entity;

import com.isoftstone.yunpan.framework.entity.BaseEntity;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/9 14:34
 * @Version: 1.0
 * @Description:
 */
public class DemoEntity extends BaseEntity {
    private String id;
    private String demoName;

    public void setId(String id) {
        this.id = id;
    }

    public void setDemoName(String demoName) {
        this.demoName = demoName;
    }

    public String getId() {
        return id;
    }

    public String getDemoName() {
        return demoName;
    }
}
