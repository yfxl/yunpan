package com.isoftstone.yunpan.system.dao;

import com.isoftstone.yunpan.framework.dao.BaseDao;
import com.isoftstone.yunpan.system.entity.EnterpriseDeptEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/12 9:37
 * @Version: 1.0
 * @Description:
 */
@Mapper
public interface EnterpriseDeptDao extends BaseDao<EnterpriseDeptEntity> {
}
