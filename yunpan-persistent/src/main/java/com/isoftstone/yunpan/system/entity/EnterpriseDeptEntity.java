package com.isoftstone.yunpan.system.entity;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.isoftstone.yunpan.framework.entity.BaseEntity;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/12 9:24
 * @Version: 1.0
 * @Description:
 * 部门信息实体
 */
public class EnterpriseDeptEntity extends BaseEntity {
    private String deptId;
    private String deptNo;
    private String deptName;
    private String createName;
    private String createDate;
    private String createTime;
    private String createUser;
    private String obsDir;
    private String companyName;

    public String getDeptNo() {
        return deptNo;
    }

    public void setDeptNo(String deptNo) {
        this.deptNo = deptNo;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getObsDir() {
        return obsDir;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public void setObsDir(String obsDir) {
        this.obsDir = obsDir;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
