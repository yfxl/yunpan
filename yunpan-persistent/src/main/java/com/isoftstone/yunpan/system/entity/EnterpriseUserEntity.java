package com.isoftstone.yunpan.system.entity;

import com.isoftstone.yunpan.framework.entity.BaseEntity;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/12 9:14
 * @Version: 1.0
 * @Description:
 * 企业用户信息实体
 */
public class EnterpriseUserEntity extends BaseEntity {
     private String userId;
     private String userno;
     private String name;
     private String email;
    private  String username;
    private  String password;
    private  String telephone;
    private  String mobilephone;
    private String obsdir;
    private String deptno;
    private  String createDate;
    private String createTime;
    private String isenable;
    private String createUser;
    private String firstTimePwd;
    private String isfirstLogin;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMobilephone() {
        return mobilephone;
    }

    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone;
    }

    public String getObsdir() {
        return obsdir;
    }

    public void setObsdir(String obsdir) {
        this.obsdir = obsdir;
    }

    public String getDeptno() {
        return deptno;
    }

    public void setDeptno(String deptno) {
        this.deptno = deptno;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getIsenable() {
        return isenable;
    }

    public void setIsenable(String isenable) {
        this.isenable = isenable;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getFirstTimePwd() {
        return firstTimePwd;
    }

    public void setFirstTimePwd(String firstTimePwd) {
        this.firstTimePwd = firstTimePwd;
    }

    public String getIsfirstLogin() {
        return isfirstLogin;
    }

    public void setIsfirstLogin(String isfirstLogin) {
        this.isfirstLogin = isfirstLogin;
    }
}
