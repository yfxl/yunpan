package com.isoftstone.yunpan.system.entity;

import com.isoftstone.yunpan.framework.entity.BaseEntity;

/**
 * @Author: zyg
 * @License: (C) Copyright 2005-2019, xxx Corporation Limited.
 * @Contact: ytzhaof@isoftstone.com
 * @Date: 2021/7/12 9:34
 * @Version: 1.0
 * @Description:
 * 资源分享信息表
 */
public class FileObjectsShareEntity extends BaseEntity {
    private String id;
    private String userId;
    private String fileName;
    private String fileURL;
    private String shareURL;
    private String shareStartTime;
    private String targetUser;
    private String shareEndTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    public String getShareURL() {
        return shareURL;
    }

    public void setShareURL(String shareURL) {
        this.shareURL = shareURL;
    }

    public String getShareStartTime() {
        return shareStartTime;
    }

    public void setShareStartTime(String shareStartTime) {
        this.shareStartTime = shareStartTime;
    }

    public String getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(String targetUser) {
        this.targetUser = targetUser;
    }

    public String getShareEndTime() {
        return shareEndTime;
    }

    public void setShareEndTime(String shareEndTime) {
        this.shareEndTime = shareEndTime;
    }
}
